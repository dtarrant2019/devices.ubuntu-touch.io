---
name: "Zuk z2 Plus"
comment: "wip"
deviceType: "phone"
maturity: .05

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/3829/zuk-z2_plus"
    icon: "yumi"
---

### Working

- Audio
- Calling
- SMS
- 4G
- Wifi (sometimes requires a reboot)
- GPS
- Vibration
- Orientation sensor
- Anbox
- Libertine
- camera
- Notification Light

### Not working

- Video recording/decoding
- Bluetooth
- Flash light
- ADB/MTP
- camera (recording)
- fingerprint
