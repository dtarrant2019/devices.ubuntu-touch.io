module.exports = function (node) {
  return {
    id: node.codename,
    title: node.name,
    date: new Date(
      node.gitData[node.gitData.length - 1].authorDate.replace(
        /\+(\w{2})(\w{2})/,
        "+$1:$2"
      )
    ),
    description: "New device added to the devices website, check it out.",
    content:
      "<p>" +
      node.name +
      " was just added to the devices page.</p>" +
      (node.description ? "<p>" + node.description + "</p>" : "") +
      "<p>" +
      node.progressStage.description +
      "</p>" +
      "<p>Added by " +
      node.gitData[node.gitData.length - 1].authorName +
      "</p>",
    image: "https://devices.ubuntu-touch.io/social-preview.jpg",
    link: "https://devices.ubuntu-touch.io" + node.path,
    author: {
      name: "UBports contributors",
      link: "https://ubports.com/"
    }
  };
};
